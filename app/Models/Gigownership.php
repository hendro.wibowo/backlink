<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Gigownership extends Model
{
    use Sortable;
    //
    public $sortable = ['id', 'code', 'verified', 'created_at'];

    public function Gig(){
        return $this->belongsTo('App\Models\Gig');
    }

}
