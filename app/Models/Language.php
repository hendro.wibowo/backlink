<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class Language extends Model
{
    use Sortable;
    //
    public $sortable = ['id', 'code', 'name','created_at'];

    public function getDisplayNameAttribute()
    {
        return $this->code ." - ". $this->name;
    }
    
    public static function getLanguageLists(){
       return Language::orderBy('code', 'ASC')->get();
    }
}
