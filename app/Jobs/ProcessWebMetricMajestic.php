<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Client;
use App\Models\Gig;

class ProcessWebMetricMajestic implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $gig;

    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Gig $gig)
    {
        $this->gig = $gig;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $stop = false;

        while (!$stop) {
            $metricUrl = 'https://seo-rank.my-addr.com/api3/8992BE363B1A9F7575CD35B7FACC54B9/'.($this->gig->website_url);
            $client = new Client();
            $result = $client->get($metricUrl);
            $responseBody = (String) $result->getBody();
            $metricResults = $responseBody != "added" || $responseBody != "progress" ? json_decode($responseBody) : false;

            if($metricResults) {
                $this->gig->ebl = floatval($metricResults->ebl);
                $this->gig->ebl_edu = floatval($metricResults->ebl_edu);
                $this->gig->ebl_gov = floatval($metricResults->ebl_gov);
                $this->gig->refd = floatval($metricResults->refd);
                $this->gig->refd_edu = floatval($metricResults->refd_edu);
                $this->gig->refd_gov = intval($metricResults->refd_gov);
                $this->gig->ips = floatval($metricResults->ips);
                $this->gig->ccs = floatval($metricResults->ccs);
                $this->gig->tf = floatval($metricResults->tf);
                $this->gig->cf = floatval($metricResults->cf);
                $this->gig->ttft0 = floatval($metricResults->ttft0);
                $this->gig->ttfv0 = floatval($metricResults->ttfv0);
                $this->gig->ttft1 = floatval($metricResults->ttft1);
                $this->gig->ttfv1 = floatval($metricResults->ttfv1);
                $this->gig->ttft2 = floatval($metricResults->ttft2);
                $this->gig->ttfv2 = floatval($metricResults->ttfv2);
                $this->gig->ttft3 = floatval($metricResults->ttft3);
                $this->gig->ttfv3 = floatval($metricResults->ttfv3);
                $this->gig->ttft4 = floatval($metricResults->ttft4);
                $this->gig->ttfv4 = floatval($metricResults->ttfv4);
                $this->gig->ttft5 = floatval($metricResults->ttft5);
                $this->gig->ttfv5 = floatval($metricResults->ttfv5);
                $this->gig->ttft6 = floatval($metricResults->ttft6);
                $this->gig->ttfv6 = floatval($metricResults->ttfv6);
                $this->gig->ttft7 = floatval($metricResults->ttft7);
                $this->gig->ttfv7 = floatval($metricResults->ttfv7);
                $this->gig->ttft8 = floatval($metricResults->ttft8);
                $this->gig->ttfv8 = floatval($metricResults->ttfv8);
                $this->gig->ttft9 = floatval($metricResults->ttft9);
                $this->gig->ttfv9 = floatval($metricResults->ttfv9);
                $this->gig->domain_status = floatval($metricResults->status);
                $this->gig->save();

                $stop = true;
            } else {
                $stop = false;

                sleep(60);
            }
        }
    }
}
