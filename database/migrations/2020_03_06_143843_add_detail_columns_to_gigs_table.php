<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailColumnsToGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gigs', function(Blueprint $table) {
            $table->integer('min_words')->default(0);
            $table->boolean('article_from_me')->default(false)->nullable();
            $table->integer('additional_price')->default(0)->nullable();
            $table->integer('days_fulfillment')->default(0)->nullable();
            $table->integer('price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gigs', function(Blueprint $table) {
            $table->dropColumn('min_words');
            $table->dropColumn('article_from_me');
            $table->dropColumn('additional_price');
            $table->dropColumn('days_fulfillment');
            $table->dropColumn('price');
        });
    }
}
