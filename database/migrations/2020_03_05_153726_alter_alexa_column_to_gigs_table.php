<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAlexaColumnToGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gigs', function(Blueprint $table) {
            $table->dropColumn('alexa');
            $table->float('alexa_rank')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gigs', function(Blueprint $table) {
            $table->dropColumn('alexa_rank');
            $table->float('alexa')->default(0)->nullable();
        });
    }
}
