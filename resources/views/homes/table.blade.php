<div class="row"> {{-- style="margin-left: 1.5rem;margin-right: 1.5rem;"> --}}
    <div class="col-12">
        <table id="backlinks_table" class="table table-striped table-bordered" style="width:100%;">
            <thead>
                <tr>
                    <th class="text-center text-black" style="width: 5% !important;">No</th>
                    <th class="text-center">Judul Blog</th>
                    <th class="text-center">DA</th>
                    <th class="text-center">PA</th>
                    <th class="text-center">Alexa</th>
                    <th class="text-center">Harga</th>
                    <th class="text-center"></th>
                </tr>
            </thead>
            <tbody>
                <tr></tr>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="detail-backlink" tabindex="-1" role="dialog" aria-labelledby="detailBacklinkModal">
    <div class="modal-dialog modal-lg" role="document" style="background-color: #fff">
        <div class="modal-content text-center" style="border-radius: unset;box-shadow: none;">
            <img src="/public/img/loading.gif" style="margin: 20px auto;" />
        </div>
        <div class="modal-footer">
            <a class="btn btn-primary" href="">Order</a>
        </div>
    </div>
</div>
<script>
$('#detail-backlink').on('show.bs.modal', function (e) {
    var url = $(e.relatedTarget).data('url');
    var orderUrl = $(e.relatedTarget).data('orderurl');
    $.get(url, function(html){
        $(e.target).find('.modal-content').removeClass('text-center').html(html);
        $(e.target).find('.modal-footer > a').attr('href', orderUrl);
    });
    $(e.target).find('.modal-content').addClass('text-center').empty().html('<img src="/public/img/loading.gif" style="margin: 20px auto;" />');
});
</script>
