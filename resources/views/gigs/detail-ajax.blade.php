<div class="row">
    <div class="col-md-12" style="padding: 0 4rem 4rem 4rem !important;">
        <h3 style="font-weight: bold">Informasi Detail Untuk: {{ $recordInfo->website_url }}</h3>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">URL Domain</div>
                    <div class="col-md-6">{{ $recordInfo->website_url }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">DA</div>
                    <div class="col-md-6">{{ $recordInfo->da }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">PA</div>
                    <div class="col-md-6">{{ $recordInfo->pa }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Mozrank</div>
                    <div class="col-md-6">{{ $recordInfo->mozrank }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Links</div>
                    <div class="col-md-6">{{ $recordInfo->links }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Equity</div>
                    <div class="col-md-6">{{ $recordInfo->equity }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Alexa Rank</div>
                    <div class="col-md-6">{{ $recordInfo->alexa_rank }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Alexa Links</div>
                    <div class="col-md-6">{{ $recordInfo->alexa_links }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Alexa CNT</div>
                    <div class="col-md-6">{{ $recordInfo->alexa_country }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Alexa CNT R</div>
                    <div class="col-md-6">{{ $recordInfo->alexa_country_rank }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Facebook Comments</div>
                    <div class="col-md-6">{{ $recordInfo->fb_comments }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Facebook Shares</div>
                    <div class="col-md-6">{{ $recordInfo->fb_shares }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Facebook Reacts</div>
                    <div class="col-md-6">{{ $recordInfo->fb_reacts }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Domain</div>
                    <div class="col-md-6">{{ $recordInfo->sr_domain }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Keywords</div>
                    <div class="col-md-6">{{ $recordInfo->sr_keywords }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Traffic</div>
                    <div class="col-md-6">{{ $recordInfo->sr_traffic }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Costs</div>
                    <div class="col-md-6">{{ $recordInfo->sr_costs }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Ulinks</div>
                    <div class="col-md-6">{{ $recordInfo->sr_ulinks }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Hlinks</div>
                    <div class="col-md-6">{{ $recordInfo->sr_hlinks }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Dlinks</div>
                    <div class="col-md-6">{{ $recordInfo->sr_dlinks }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">SEO Rank Dlinks</div>
                    <div class="col-md-6">{{ $recordInfo->sr_dlinks }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Domain Status</div>
                    <div class="col-md-6">{{ $recordInfo->domain_status }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">External Backlinks (EBL)</div>
                    <div class="col-md-6">{{ $recordInfo->ebl }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">External Backlinks EDU</div>
                    <div class="col-md-6">{{ $recordInfo->ebl_edu }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">External Backlinks GOV</div>
                    <div class="col-md-6">{{ $recordInfo->ebl_gov }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Referring Domains (RefD)</div>
                    <div class="col-md-6">{{ $recordInfo->refd }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Referring Domains EDU (RefD_EDU)</div>
                    <div class="col-md-6">{{ $recordInfo->refd_edu }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Referring Domains GOV (RefD_GOV)</div>
                    <div class="col-md-6">{{ $recordInfo->refd_gov }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">IP Addresses (IPS)</div>
                    <div class="col-md-6">{{ $recordInfo->ips }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Class C Subnets (CCS)</div>
                    <div class="col-md-6">{{ $recordInfo->ccs }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Trust Flow (TF)</div>
                    <div class="col-md-6">{{ $recordInfo->tf }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Citation Flow (CF)</div>
                    <div class="col-md-6">{{ $recordInfo->cf }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 0 (TTFT0)</div>
                    <div class="col-md-6">{{ $recordInfo->ttft0 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 0 (TTFV0)</div>
                    <div class="col-md-6">{{ $recordInfo->ttfv0 }}</div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 1 (TTFT1)</div>
                    <div class="col-md-6">{{ $recordInfo->ttft1 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 1 (TTFV1)</div>
                    <div class="col-md-6">{{ $recordInfo->ttfv1 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 2 (TTFT2)</div>
                    <div class="col-md-6">{{ $recordInfo->ttft2 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 2 (TTFV2)</div>
                    <div class="col-md-6">{{ $recordInfo->ttfv2 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 3 (TTFT3)</div>
                    <div class="col-md-6">{{ $recordInfo->ttft3 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 3 (TTFV3)</div>
                    <div class="col-md-6">{{ $recordInfo->ttfv3 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 4 (TTFT4)</div>
                    <div class="col-md-6">{{ $recordInfo->ttft4 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 4 (TTFV4)</div>
                    <div class="col-md-6">{{ $recordInfo->ttfv4 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 5 (TTFT5)</div>
                    <div class="col-md-6">{{ $recordInfo->ttft5 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 5 (TTFV5)</div>
                    <div class="col-md-6">{{ $recordInfo->ttfv5 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 6 (TTFT6)</div>
                    <div class="col-md-6">{{ $recordInfo->ttft6 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 6 (TTFV6)</div>
                    <div class="col-md-6">{{ $recordInfo->ttfv6 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 7 (TTFT7)</div>
                    <div class="col-md-6">{{ $recordInfo->ttft7 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 7 (TTFV7)</div>
                    <div class="col-md-6">{{ $recordInfo->ttfv7 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 8 (TTFT8)</div>
                    <div class="col-md-6">{{ $recordInfo->ttft8 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 8 (TTFV8)</div>
                    <div class="col-md-6">{{ $recordInfo->ttfv8 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Topic 9 (TTFT9)</div>
                    <div class="col-md-6">{{ $recordInfo->ttft9 }}</div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="font-weight: bold; padding-right: 0">Topical Trust Flow Value 9 (TTFV9)</div>
                    <div class="col-md-6">{{ $recordInfo->ttfv9 }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
