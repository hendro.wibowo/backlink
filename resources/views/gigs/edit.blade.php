@extends('layouts.inner')
@section('content')
{{ HTML::script('public/js/front/tokenize2.js')}}
{{ HTML::script('public/js/ckeditor/ckeditor.js')}}
<?php
if ($gigOverviewData->status == 1) {
    $is_step_1 = 'current';
    $is_step_2 = '';
    $stepcount = 1;
    $disable_button = 'step_2';
    $show_step_id = 'bl_step_1';
} else {
    $is_step_1 = '';
    $is_step_2 = 'current';
    $stepcount = $gigOverviewData->current_step;
    $disable_button = 'step_'.($stepcount + 1);
    $show_step_id = 'bl_step_'.$stepcount;
}
// dd($gigOverviewData);
?>
<script>
$(document).ready(function () {

});
</script>
<script>

    function hideerrorsucc() {
        $('.close.close-sm').click();
    }
    $(window).load(function () {
        $("#<?php echo $disable_button; ?>").prop("disabled", false);
    });
    $(document).ready(function () {

        $.validator.addMethod("yturl", function (value, element) {
            return  this.optional(element) || /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);
        }, "Youtube video url is not valid!");

        // CKEDITOR.replace( 'description', {
        //     toolbar :
        //         [
        //             ['ajaxsave'],
        //             ['Styles','Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-'],
        //             ['Cut','Copy','Paste','PasteText'],
        //             ['Undo','Redo','-','RemoveFormat'],
        //             ['TextColor','BGColor'],
        //             ['Maximize', 'Image', 'Table','Link', 'Unlink']
        //     ],
        //     filebrowserUploadUrl : '<?php echo HTTP_PATH;?>/admin/pages/pageimages',
        //     language: '',
        //     height: 300,
        //     //uiColor: '#884EA1'
        // });

        $('.step_form_inner').hide();
        $('#<?php echo $show_step_id; ?>').show();


        $("#gigform").validate({
            submitHandler: function (form) {
                var step = $('#stepcnt').val();

                $('input[type=checkbox]:checked').each(function () {
                    $(this).val(1);
                });

                hideerrorsucc();

                if (step == 1) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo HTTP_PATH; ?>/gigs/edit/<?php echo $gigOverviewData->slug; ?>",
                        data: $('#gigform').serialize(),
                        cache: false,
                        beforeSend: function () {
                            $('#loaderID').show();
                        },
                        success: function (data) {

                            $('#loaderID').hide();
                            is_error = '0';
                            err_html = '';

                            jQuery.each(data.errors, function(key, value){
                                if(value != ''){
                                    is_error = 1;
                                    err_html = err_html +value+'<br/>';
                                }
                            });

                            if(is_error == '1'){
                                    jQuery('.er_msg').append('<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>'+err_html+'</div>');
                                    setTimeout("hideerrorsucc()", 4000);
                            } else {
                                jQuery.each(data.message, function(key, value){
                                    jQuery('.er_msg').append('<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>'+value+'</div>');
                                    // setTimeout("hideerrorsucc()", 4000);
                                });


                                $('#stepcnt').val('2');
                                $(".step_form_inner").hide();
                                $(".step_account2").show();

                                $(".valid").removeClass('current');
                                $("#tab_step_1,#tab_step_2").addClass('current');
                            }
                        },
                        error: function (data) {
                            $('#loaderID').hide();
                            jQuery.each(data.errors, function(key, value){
                                is_error = 1;
                                err_html = err_html +value+'<br/>';
                            });
                            if(is_error == '1'){
                                jQuery('.er_msg').append('<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>'+err_html+'</div>');
                                setTimeout("hideerror()", 4000);
                            }
                        }
                    });

                } else if (step == 2) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo HTTP_PATH; ?>/gigs/edit/<?php echo $gigOverviewData->slug; ?>",
                        data: $('#gigform').serialize(),
                        cache: false,
                        beforeSend: function () {
                            $('#loaderID').show();
                        },
                        success: function (data) {
                            $('#loaderID').hide();
                            is_error = 0;
                            err_html = '';
                            jQuery.each(data.errors, function(key, value){
                                if(value != ''){
                                    is_error = 1;
                                    err_html = err_html +value+'<br/>';
                                }

                            });
                            if(is_error == '1'){
                                    jQuery('.er_msg').append('<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>'+err_html+'</div>');
                                    setTimeout("hideerrorsucc()", 4000);
                            } else {
                                jQuery.each(data.message, function(key, value){
                                    jQuery('.er_msg').append('<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>'+value+'</div>');
                                    // setTimeout("hideerrorsucc()", 4000);
                                });

                                var verifyCode = data.data.code;
                                var encryptedVerifyCode = data.data.encrypted_code;

                                $('#stepcnt').val('3');
                                $(".step_form_inner").hide();
                                $(".step_account3").show();
                                $(".step_account3").html($(".step_account3").html().replace(/\|\|verify_code\|\|/g, verifyCode));
                                $(".step_account3").html($(".step_account3").html().replace(/\|\|enc_verify_code\|\|/g, encryptedVerifyCode));
                                $(".current").removeClass('current');
                                $("#tab_step_1,#tab_step_2,#tab_step_3").addClass('current');
                            }
                        },
                        error: function (data) {
                            $('#loaderID').hide();
                            jQuery.each(data.errors, function(key, value){
                                is_error = 1;
                                err_html = err_html +value+'<br/>';
                            });
                            if(is_error == '1'){
                                jQuery('.er_msg').append('<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>'+err_html+'</div>');
                                setTimeout("hideerror()", 4000);
                            }
                        }
                    });
                } else if (step == 3) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo HTTP_PATH; ?>/gigs/edit/<?php echo $gigOverviewData->slug; ?>",
                        data: $('#gigform').serialize(),
                        cache: false,
                        beforeSend: function () {
                            $('#loaderID').show();
                        },
                        success: function (data) {
                            $('#loaderID').hide();
                            is_error = 0;
                            err_html = '';
                            jQuery.each(data.errors, function(key, value){
                                if(value != ''){
                                    is_error = 1;
                                    err_html = err_html +value+'<br/>';
                                }

                            });
                            if(is_error == '1'){
                                    jQuery('.er_msg').append('<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>'+err_html+'</div>');
                                    setTimeout("hideerrorsucc()", 4000);
                            } else {
                                jQuery.each(data.message, function(key, value){
                                    jQuery('.er_msg').append('<div class="alert alert-success fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>'+value+'</div>');
                                    // setTimeout("hideerrorsucc()", 4000);
                                });
                                $('#stepcnt').val('4');
                                $(".step_form_inner").hide();
                                $(".step_account4").show();
                                $(".current").removeClass('current');
                                $("#tab_step_1,#tab_step_2,#tab_step_3,#tab_step_4").addClass('current');
                            }
                        },
                        error: function (data) {
                            console.log(data);
                            $('#loaderID').hide();
                            jQuery.each(data.errors, function(key, value){
                                is_error = 1;
                                err_html = err_html +value+'<br/>';
                            });
                            if(is_error == '1'){
                                jQuery('.er_msg').append('<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close close-sm" type="button"><i class="fa fa-times"></i></button>'+err_html+'</div>');
                                setTimeout("hideerror()", 4000);
                            }
                        }
                    });
                } else {
                    if (confirm('Are you sure to publish this backlink?')) {
                        form.submit();
                        return true;
                    } else {
                        return false;
                    }

                }
            }
        });


        $("#backstep_1").click(function () {
            $('#stepcnt').val('1');
            $(".step_form_inner").hide();
            $(".step_account1").show();

            $(".current").removeClass('current');
            $("#tab_step_1").addClass('current');
        });
        $("#backstep_2").click(function () {
            $('#stepcnt').val('2');
            $(".step_form_inner").hide();
            $(".step_account2").show();

            $(".current").removeClass('current');
            $("#tab_step_1,#tab_step_2").addClass('current');
        });
        $("#backstep_3").click(function () {
            $('#stepcnt').val('3');
            $(".step_form_inner").hide();
            $(".step_account3").show();

            $(".current").removeClass('current');
            $("#tab_step_1,#tab_step_2,#tab_step_3").addClass('current');
        });
        $("#backstep_4").click(function () {
            $('#stepcnt').val('3');
            $(".step_form_inner").hide();
            $(".step_account3").show();
            $(".current").removeClass('current');
            $("#tab_step_1,#tab_step_2,#tab_step_3").addClass('current');
        });
        $("#category_id").change(function () {
            var catid = $("#category_id").val();
            if(catid == ''){
                catid = '0';
            }
            $("#subcategory").load('<?php echo HTTP_PATH . '/gigs/getsubcategorylist/' ?>' + catid);
        });
    });
</script>
<div class="main_dashboard">
    <div class="dashboard-menu">
        <div class="navbar navbar-default">
            <nav class="navbar navbar-me">
                <div class="container">
                    <div class="nevicatio-menu">
                        <ul class="top_tab" data-mode="wizard">
                            <li class="step hint--bottom">
                                <a href="#!" id="tab_step_1" class="valid <?php echo $stepcount == 1 || $stepcount == 2 || $stepcount == 3 || $stepcount == 4 ? 'current' : ''; ?>" >
                                    <span>1</span> Overview
                                </a>
                            </li>
                            <li class="step hint--bottom">
                                <a href="#!" id="tab_step_2" class="valid <?php echo $stepcount == 2 || $stepcount == 3 || $stepcount == 4 ? 'current' : ''; ?>">
                                    <span>2</span> Details
                                </a>
                            </li>
                            <li class="step hint--bottom">
                                <a href="#!" id="tab_step_3" class="valid <?php echo $stepcount == 3 || $stepcount == 4 ? 'current' : ''; ?>" >
                                    <span>3</span> Proof of Ownership
                                </a>
                            </li>
                            <li class="step hint--bottom">
                                <a href="#!" id="tab_step_4" class="valid <?php echo $stepcount == 4 ? 'current' : ''; ?>" > <span>4</span> Publish</a>
                            </li>
                        </ul>

                    </div>
                </div>
            </nav>
        </div>
    </div>
    <section class="dashboard-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ee er_msg">@include('elements.errorSuccessMessage')</div>
                    <div class="gig_from">
                        {{Form::model($gigOverviewData, ['method' => 'post', 'name' => 'gigform', 'id' => 'gigform', 'enctype' => "multipart/form-data"]) }}
                            <input type="hidden" value="<?php echo $stepcount; ?>" name="stepcnt" id="stepcnt" />
                            <div class="step_form_inner step_account1" id="bl_step_1">
                                {{-- <div class="form_field">
                                    <label>Website Title</label>
                                    <div class="right_filed">
                                        <div class="text_area">
                                            {{Form::text('title', $gigOverviewData->title, ['minlength' => 5, 'maxlength' => 80, 'class' => 'form-control required', 'placeholder' => "Your website title", 'autocomplete' => 'off'])}}
                                            <!--<span class="first_txt">i will</span>-->
                                        </div>
                                        <figure class="textareatooltip">
                                            <figcaption>
                                                <h3>Put your website title.</h3>
                                                <p>This is your website title.</p>
                                            </figcaption>
                                            <div class="gig-tooltip-img"></div>
                                        </figure>
                                    </div>
                                </div> --}}
                                <div class="form_field">
                                    <label>URL</label>
                                    <div class="right_filed">
                                        <div class="text_area">
                                            {{Form::text('website_url', $gigOverviewData->website_url, ['minlength' => 5, 'maxlength' => 80, 'class' => 'form-control required', 'placeholder' => "Your website URL", 'autocomplete' => 'off'])}}
                                            <!--<span class="first_txt">i will</span>-->
                                        </div>
                                        <div class="tag_tooltip">
                                            <div class="fake-hint blue">
                                                <div class="icn">
                                                    <i class="fa fa-lightbulb-o"></i>
                                                </div>
                                                <span class="fake-hint-title">
                                                    Your website URL. Must include <strong>https://</strong> or <strong>http://</strong>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_field">
                                    <label>Category
                                        <!--<a href="#">Upgrade SEO</a>-->
                                    </label>
                                    <div class="right_filed tg_bx">
                                        <div class="text_input">
                                            <select class="tokenize-custom-demo1" multiple name="categories[]">
                                                <?php
                                                $categories = $gigOverviewData->Category()->pluck('id')->toArray();
                                                if ($skills) {
                                                    foreach ($skills as $id => $skillsVal) {
                                                        ?> <option value="<?php echo $skillsVal; ?>" {{ in_array($id, $categories) ? 'selected' : '' }}><?php echo $skillsVal; ?></option><?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="tag_tooltip">
                                            <div class="fake-hint blue">
                                                <div class="icn">
                                                    <i class="fa fa-lightbulb-o"></i>
                                                </div>
                                                <span class="fake-hint-title">Enter search terms, which you feel buyers will use when looking for your service. The terms you enter here are very important and will have an impact on your overall exposure on {{SITE_TITLE}}. When adding your search terms, please keep in mind the following:</span>
                                                <ul>
                                                    <li>Special characters and duplicated terms will be ignored.</li>
                                                    <li>It doesn’t matter if you use upper case, lower case letters, or plural forms of words.</li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_field">
                                    <label>Language
                                    </label>
                                    <div class="right_filed tg_bx">
                                        <div class="text_input">
                                            <select class="tokenize-custom-demo1" multiple name="languages[]">
                                                <?php
                                                $langData = explode(',', $gigOverviewData->languages);
                                                if ($languages) {
                                                    foreach ($languages as $languagesVal) {
                                                        ?> <option value="<?php echo $languagesVal->code; ?>" {{ in_array($languagesVal->id, $langData) ? 'selected' : '' }}><?php echo $languagesVal->display_name; ?></option><?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="tag_tooltip">
                                            <div class="fake-hint blue">
                                                <div class="icn">
                                                    <i class="fa fa-lightbulb-o"></i>
                                                </div>
                                                <span class="fake-hint-title">Enter search terms, to find your website languages. You can choose max 5 languages.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="page_btn page_btn_creat">
                                    <a href="{{ URL::to( 'users/dashboard')}}" title="Cancel" class="btn btn-default cancel_btn">Cancel</a>
                                    {{Form::submit('Save &amp; Continue', ['class' => 'btn btn-info', 'id'=>'step_2'])}}
                                    <div class="gig_loader" id="ovw_ldr" style="display: none;" id="loaderID">{{HTML::image("public/img/loading.gif", SITE_TITLE)}}</div>
                                </div>
                            </div>
                            {{-- {{dd($gigOverviewData)}} --}}
                            <div class="step_form_inner second_page step_account2" id="bl_step_2">
                                <div class="form_field">
                                    <label>Min. Words</label>
                                    <div class="right_filed tg_bx half_field">
                                        <div class="text_input">
                                            {{Form::number('min_words', isset($gigOverviewData->min_words) ? $gigOverviewData->min_words : '', ['style' => 'width: 60%', 'class' => 'form-control required',])}}
                                            <!--<span class="first_txt">i will</span>-->
                                        </div>
                                        <div class="tag_tooltip">
                                            <div class="fake-hint blue">
                                                <div class="icn">
                                                    <i class="fa fa-lightbulb-o"></i>
                                                </div>
                                                <span class="fake-hint-title">Minimum words.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form_field">
                                    <label>Price (USD)</label>
                                    <div class="right_filed tg_bx half_field">
                                        <div class="text_input">
                                            {{Form::number('price', isset($gigOverviewData->price) ? $gigOverviewData->price : '', ['style' => 'width: 30%', 'class' => 'form-control required',])}}
                                            <!--<span class="first_txt">i will</span>-->
                                        </div>
                                        <div class="tag_tooltip">
                                            <div class="fake-hint blue">
                                                <div class="icn">
                                                    <i class="fa fa-lightbulb-o"></i>
                                                </div>
                                                <span class="fake-hint-title">Your backlink price in USD.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form_field">
                                    <label>Article From Me [?]</label>
                                    <div class="right_filed tg_bx half_field">
                                        <div class="text_input" style="padding-top: 7px;">
                                            {{Form::checkbox('article_from_me', 1, isset($gigOverviewData->article_from_me) ? $gigOverviewData->article_from_me : false)}}
                                            <!--<span class="first_txt">i will</span>-->
                                        </div>
                                        <div class="tag_tooltip">
                                            <div class="fake-hint blue">
                                                <div class="icn">
                                                    <i class="fa fa-lightbulb-o"></i>
                                                </div>
                                                <span class="fake-hint-title">You also provide article or content writing service with additional costs in USD.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(isset($gigOverviewData->article_from_me))
                                    @if(intval($gigOverviewData->article_from_me) == 0)
                                    <div class="form_field" id="additional_price_container" style="display:none">
                                    @else
                                    <div class="form_field" id="additional_price_container">
                                    @endif
                                @else
                                <div class="form_field" id="additional_price_container" style="display:none">
                                @endif
                                    <label>Additional Price (USD)</label>
                                    <div class="right_filed tg_bx half_field">
                                        <div class="text_input">
                                            {{Form::number('additional_price', isset($gigOverviewData->additional_price) ? $gigOverviewData->additional_price : '', ['style' => 'width: 30%', 'class' => 'form-control',])}}
                                            <!--<span class="first_txt">i will</span>-->
                                        </div>
                                        <div class="tag_tooltip">
                                            <div class="fake-hint blue">
                                                <div class="icn">
                                                    <i class="fa fa-lightbulb-o"></i>
                                                </div>
                                                <span class="fake-hint-title">Additional costs in USD.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <script>
                                $('[name="article_from_me"]').change(function(e){
                                    if($(e.target).prop('checked')) {
                                        $('#additional_price_container').slideDown();
                                    } else {
                                        $('#additional_price_container').slideUp();
                                    }
                                })
                                </script>

                                <div class="form_field">
                                    <label>Fulfillment Days</label>
                                    <div class="right_filed tg_bx half_field">
                                        <div class="text_input">
                                            {{Form::number('days_fulfillment', isset($gigOverviewData->days_fulfillment) ? $gigOverviewData->days_fulfillment : '', ['style' => 'width: 30%', 'class' => 'form-control required',])}}
                                            <!--<span class="first_txt">i will</span>-->
                                        </div>
                                        <div class="tag_tooltip">
                                            <div class="fake-hint blue">
                                                <div class="icn">
                                                    <i class="fa fa-lightbulb-o"></i>
                                                </div>
                                                <span class="fake-hint-title">How many days the backlink will be ready.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="page_btn page_btn_creat">
                                    {{Form::button('Back', ['class' => 'cancel_btn', 'id'=>'backstep_1'])}}
                                    {{Form::submit('Save &amp; Continue', ['class' => 'btn btn-info', 'id'=>'step_3'])}}
                                    <div class="gig_loader" id="pkg_ldr" style="display: none;" id="loaderID">{{HTML::image("public/img/loading.gif", SITE_TITLE)}}</div>
                                </div>
                            </div>

                            <div class="step_form_inner third_page step_account3" id="bl_step_3">
                                {{-- @if(isset($gigOwnership->code)) --}}
                                <div class="form_field">
                                    <h4>
                                        Prove that you own <a href="{{ $gigOverviewData->website_url }}" target="_blank" style="font-weight:bold">{{ str_replace(['https://', 'http://'], '', rtrim($gigOverviewData->website_url, '/')) }}</a>!
                                    </h4>
                                    <p>&nbsp;</p>
                                    <p>1. Download this <a href="{{ route('gig.verification-file', array('slug' => $gigOverviewData->slug)) }}" style="font-weight:bold">txt file</a> and upload to your root folder.</p>
                                    <p>2. Copy this code snippet before &lt;body&gt;:</p>
                                    <p style="margin-left:10px;padding:10px;background-color: #eee; overflow: scroll;">
                                        <code>
                                        @php
                                        $snippet = '<script>var a=new XMLHttpRequest;"withCredentials"in a||(a=new XDomainRequest);a.open("GET","'.config('app.url').'/verify-client-js?code=||enc_verify_code||");a.onload=function(b){console.log(b.currentTarget.response||b.target.responseText)};a.send();
</script>';
                                        echo htmlentities($snippet);
                                        @endphp
                                        </code>
                                    </p>
                                    <p>
                                        3. If you do not have access to root folder on your server or your website source code, then you can put <strong>"{{ isset($gigOwnership->code) ? $gigOwnership->code : '||verify_code||' }}"</strong> (includes the doublequotes) text to any visible parts of your website.
                                           For example, You can create a post with title or body containing this text: <strong>"{{ isset($gigOwnership->code) ? $gigOwnership->code : '||verify_code||' }}"</strong> (includes the doublequotes).
                                    </p>
                                </div>

                                <div class="page_btn page_btn_creat">
                                    {{Form::button('Back', ['class' => 'cancel_btn', 'id'=>'backstep_2'])}}
                                    {{Form::submit('Verify &amp; Continue', ['class' => 'btn btn-info', 'id'=>'step_4'])}}
                                    <div class="gig_loader" id="pkg_ldr" style="display: none;" id="loaderID">{{HTML::image("public/img/loading.gif", SITE_TITLE)}}</div>
                                </div>
                                {{-- @endif --}}
                            </div>

                            <div class="step_form_inner fifth_page step_account4" id="bl_step_4">
                                <div class="congratulation_content">
                                    <h2>Congratulations!</h2>
                                    <h4>You're all set. One more step to publish your listing. Click submit below.<br>
                                </div>
                                <div class="simple_txt">If you agree with our <a href="javascript:void(0);" onclick="window.open('<?php echo HTTP_PATH ?>/privacy-policy', 'term', 'width=900,height=400,scrollbars=1')" >Privacy Policy</a> than please click on Save and Publish button</div>
                                <div class="page_btn">
                                    {{Form::button('Back', ['class' => 'cancel_btn', 'id'=>'backstep_4'])}}
                                    {{Form::submit('Submit & Publish', ['class' => 'btn btn-info', 'id'=>'step_5'])}}
                                    <div class="gig_loader" id="pub_ldr" style="display: none;" id="loaderID">{{HTML::image("public/img/loading.gif", SITE_TITLE)}}</div>
                                </div>
                            </div>
                        {{ Form::close()}}
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script>
    function open_accordion_section(val) {
        //alert(val);
        var currentAttrValue = '#accordion-' + val;

        if ($('#accordion-section-title-' + val).is('.active')) {
            close_accordion_section();
        } else {
            close_accordion_section();

            // Add active class to section title
            $('#accordion-section-title-' + val).addClass('active');
            // Open up the hidden content panel
            $('.question_accordion ' + currentAttrValue).slideDown(300).addClass('open');
        }
    }
    function close_accordion_section() {
        jQuery('.question_accordion .accordion-section-title').removeClass('active');
        jQuery('.question_accordion .accordion-section-content').slideUp(300).removeClass('open');
    }

    function addmoreextra() {

        var timestamp = Number(new Date()); // current time as number
        newItem = timestamp;
        var faqnav = '<div id="extra_box_' + newItem + '" class="answer_div answer_div_new">';
        faqnav += '<a class="delete-btn" href="javascript:void(0)" onclick="deleteextra(' + newItem + ')"><i class="fa fa-trash"></i></a>';
        faqnav += '<div class="gigs-inputs"><input id="faqextttl_' + newItem + '" class="form-control faqques required" name="exttitle[' + newItem + ']" minlength="5" maxlength="80" placeholder="Title your extra service" autocomplete="OFF" type="text"></div>';
        faqnav += '<div class="gigs-inputs"><input id="faqextdes_' + newItem + '" class="form-control faqans required" name="extdescription[' + newItem + ']" minlength="5" maxlength="80" placeholder="Describe your offering" autocomplete="OFF" type="text"></div>';
        faqnav += '<div class="gigs-inputs gigs-inputs-select"><div class="drop_arow"><div class="market-select"><span>';
        faqnav += '<select id="faqextdel_' + newItem + '" class="form-control required" name="extdelivery[' + newItem + ']">';
        faqnav += '<option selected="selected" value="">Delivery Time</option>';
        for(var i=1;i<30;i++){
            faqnav += '<option value='+i+'>'+i+' Day Delivery</option>';
        }
        faqnav += '</select>';
        faqnav += '</span></div></div></div>';
        faqnav += '<div class="gigs-inputs gigs-inputs-select"><div class="drop_arow"><div class="market-select"><span>';
        faqnav += '<select id="faqextprc_' + newItem + '" class="form-control required" name="extprice[' + newItem + ']">';
        faqnav += '<option selected="selected" value="">Select Price</option>';
        for(p = 5;p < 1000;){
            faqnav += '<option value='+p+'>{{CURR}}'+p+'</option>';
            p = parseInt(p)+5;
        }
        faqnav += '</select>';
        faqnav += '</span></div></div></div>';
        faqnav += '</div>';
            $(".text_fileld_wrap_ext").append(faqnav);
    }

    function addmorerequirement() {

        var timestamp = Number(new Date()); // current time as number
        newItem = timestamp;
        var faqnav = '<div id="requirement_box_' + newItem + '" class="answer_div">';
        faqnav += '<label class="req_lbl">REQUIREMENT #' + newItem + '</label>';
        faqnav += '<a href="javascript:void(0)" onclick="deleterequirement(' + newItem + ')"><i class="fa fa-trash"></i></a>';
        faqnav += '<textarea name="reqdescription[' + newItem + ']" id="faqreq_' + newItem + '" minlength="20" maxlength="450" class="faqans texta required valid" placeholder="For example: specifications, dimensions, brand guidelines, or background materials."></textarea>';
        faqnav += '<div class="textarea_active">';
        faqnav += '<div class="answer">';
        faqnav += '<input type="checkbox" name="is_mandatory[' + newItem + ']" id="faqmand_' + newItem + '" class="css-checkbox in-checkbox">';
        faqnav += '<label class="in-label" for="faqmand_' + newItem + '">Answer is mandatory</label>';
        faqnav += '</div>';
        faqnav += '</div>';
        faqnav += '</div>';
        $(".text_fileld_wrap_req").append(faqnav);

        setRequirementNumbering();
    }
    function setRequirementNumbering() {
        var total_requirement = $('.req_lbl').length;
        for (var r = 0; r < total_requirement; r++) {
            var r_no = r + 1;
            $(".req_lbl:eq(" + r + ")").html("REQUIREMENT #" + r_no);
        }
    }
</script>

<script>
    function in_array(needle, haystack) {
        for (var i = 0, j = haystack.length; i < j; i++) {
            if (needle == haystack[i])
                return true;
        }
        return false;
    }

    function getExt(filename) {
        var dot_pos = filename.lastIndexOf(".");
        if (dot_pos == -1)
            return "";
        return filename.substr(dot_pos + 1).toLowerCase();
    }

    function imageValidation(imageId) {
        $('#no_image_div').css("display", "none");
        $('#selectIcon').css("display", "none");
        $('#undoIcon').css("display", "block");
        var filename = document.getElementById(imageId).value;
        var filetype = ['jpg', 'jpeg', 'png', 'gif'];
        if (filename != '') {
            var ext = getExt(filename);
            ext = ext.toLowerCase();
            var checktype = in_array(ext, filetype);
            if (!checktype) {
                alert(ext + " file not allowed.");
                document.getElementById(imageId).value = '';
                return false;
            } else {
                var fi = document.getElementById(imageId);
                var filesize = fi.files[0].size;//check uploaded file size
                if (filesize > 8388608) {
                    alert('Maximum 8MB file size allowed.');
                    document.getElementById(imageId).value = '';
                    return false;
                }
            }
        }
    }
    function pdfValidation(imageId) {
        $('#no_image_div').css("display", "none");
        $('#selectIcon').css("display", "none");
        $('#undoIcon').css("display", "block");
        var filename = document.getElementById(imageId).value;
        var filetype = ['pdf'];
        if (filename != '') {
            var ext = getExt(filename);
            ext = ext.toLowerCase();
            var checktype = in_array(ext, filetype);
            if (!checktype) {
                alert(ext + " file not allowed.");
                document.getElementById(imageId).value = '';
                return false;
            } else {
                var fi = document.getElementById(imageId);
                var filesize = fi.files[0].size;//check uploaded file size
                if (filesize > 8388608) {
                    alert('Maximum 8MB file size allowed.');
                    document.getElementById(imageId).value = '';
                    return false;
                }
            }
        }
    }

</script>
<script>

    function deleteimage(id){
       if(id != ""){
            $.ajax({
                type:'GET',
                 url: "<?php echo HTTP_PATH; ?>/gigs/deleteimage/"+id,
                //data:{'files': $('#attachmentfiles').val()},
                cache:false,
                beforeSend: function () {
                    //NProgress.start();
                },
                success:function(data){
                   // NProgress.done();

                },
                error: function(data){
                    console.log("error");
                    console.log(data);
                }
            });
       }
}

function deletefile(id){
       $('#'+id).remove();
       var img = $('#'+id).attr('data-img');
       var attachmentfiles = $('#attachmentfiles').val();
       if(attachmentfiles != ""){
           var imgs = attachmentfiles.split(',');
           imgs.splice( $.inArray(img,imgs) ,1 );
           $('#attachmentfiles').val(imgs.join(','));
            $.ajax({
                type:'POST',
                 url: "<?php echo HTTP_PATH; ?>/gigs/updatedocument/<?php echo $gigOverviewData->id ?>",
                data:{'files': $('#attachmentfiles').val()},
                cache:false,
                 beforeSend: function () {
                    NProgress.start();
                },
                success:function(data){
                    NProgress.done();

                },
                error: function(data){
                    console.log("error");
                    console.log(data);
                }
            });
       }
}
    function uploadLogo() {
        var filename = document.getElementById("add_logo").value;
        var filetype = ['doc', 'pdf', 'docx'];
        if (filename != '') {
            var ext = getExt(filename);
            ext = ext.toLowerCase();
            var checktype = in_array(ext, filetype);
            if (!checktype) {
                alert(ext + " file not allowed for Attachment.");
                document.getElementById("add_logo").value = '';
                return false;
            } else {
                var fi = document.getElementById('add_logo');
                var filesize = fi.files[0].size;//check uploaded file size
                if (filesize > 2097152) {
                    alert('Maximum 2MB file size allowed for Attachment.');
                    document.getElementById("add_logo").value = '';
                    return false;
                }
            }
        }
        $("#isdocupload").val('1');
        $("#gigform").submit();
        return true;
    }
</script>
<script>
$(document).ready(function (e) {

        $("#add_logo").on("change", function () {
            uploadLogo();

        });
    });
</script>

<script type="text/javascript">
    $('.tokenize-custom-demo1').tokenize2({
        tokensAllowCustom: true
    });
</script>
@endsection
