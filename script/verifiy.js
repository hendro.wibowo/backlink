function verifyYourWebsite(host, code, success) {
    var xhr = new XMLHttpRequest();
    var url = host + '/verify-client-js?code=' + code;
    if (!('withCredentials' in xhr)) xhr = new XDomainRequest(); // fix IE8/9
    xhr.open('GET', url);
    xhr.onload = success;
    xhr.send();
    return xhr;
}

verifyYourWebsite('[[host]]', '[[code]]', function(request){
    var response = request.currentTarget.response || request.target.responseText;
    console.log(response);
});
